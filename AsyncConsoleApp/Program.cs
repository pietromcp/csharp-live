﻿public static class Program {
    private static int value = 19;

    private static int DoubleValue {
        get { return value * 2; }
    }

    public static async Task Main() {
        Console.WriteLine(value);
        await Task.Delay(1000);
        var rInt = await GetIntValueAsync();
        Console.WriteLine(rInt);
        var rLong = await GetLongValueAsync();
        Console.WriteLine(rLong);
        File.WriteAllText("test0.txt", @"File scritto con API sincrona");
        await File.WriteAllTextAsync("test1.txt", @"File scritto con API Asincrona");
    }
    
    public static void OldMain() {
        Task.Delay(1000)
            .ContinueWith(t => {
                GetIntValueAsync()
                    .ContinueWith(t1 => {
                        Console.WriteLine(t1.Result);
                        GetLongValueAsync()
                            .ContinueWith(t2 => {
                                Console.WriteLine(t2.Result);
                            });
                    });
            });
    }

    static Task<int> GetIntValueAsync() {
        value++;
        Console.WriteLine(value);
        return Task.FromResult(19);
    }
    
    static Task<long> GetLongValueAsync() {
        return Task.FromResult(19L);
    }
}