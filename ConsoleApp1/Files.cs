namespace ConsoleApp1; 

public class Files {
    public static void Run() {
        var text = @"
Nel mezzo del cammin di nostra vita
mi ritrovai per una selva oscura
...
";
        const string fileNameI = "inferno-I.txt";
        // File.WriteAllText(fileName, text);

        var lines = File.ReadAllLines(fileNameI);
        foreach (var line in lines) {
            Console.WriteLine("## " + line);
        }

        var stream = File.OpenRead(fileNameI);
        stream.Seek(15, SeekOrigin.Begin);
        var reader = new StreamReader(stream);
        string line2 = null;
        while ((line2 = reader.ReadLine()) != null) {
            Console.WriteLine("-- " + line2);
        }

        var outStream = File.OpenWrite("orlando-I.txt");
        using var writer = new StreamWriter(outStream);
        writer.WriteLine("Le donne, i cavalier, l'arme, gli amori");
        writer.WriteLine("le cortesie, l'audaci imprese io canto");
        writer.WriteLine("che fuoro al tempo che passaro i mori d'Africa il mare");
        writer.WriteLine("...");
        // outStream.Close();

        using var myVar = new MyUsableClass();
        Console.WriteLine("Corpo dello statement using 0");
        Console.WriteLine("Corpo dello statement using 1");
        Console.WriteLine("Corpo dello statement using 2");
        Console.WriteLine("Fuori dallo statement using");
    }

    public async Task ExecuteAsync() {
        await using var myVar = new MyUsableClass();
    }
}

class MyUsableClass : IDisposable, IAsyncDisposable {
    public void Dispose() {
        Console.WriteLine("MyUsableClass.Dispose");
    }

    public ValueTask DisposeAsync() {
        throw new NotImplementedException();
    }
}
