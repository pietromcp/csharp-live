namespace ConsoleApp1.Accounts; 

public class UserAccount {
    public string UserId { get; set; }
    public string Username { get; set; }
}

public interface UserAccountRepository {
    string Save(UserAccount account);

    UserAccount Load(string id);

    IList<UserAccount> GetAll();
}
/*
public class OracleUserAccountRepository : UserAccountRepository {
}

public class MongoDbUserAccountRepository : UserAccountRepository {
}

public class InMemoryUserAccountRepository : UserAccountRepository {
}
*/
public class UserAccountBusinessLogic {
    private UserAccountRepository repo;

    public UserAccountBusinessLogic(UserAccountRepository repo) {
        this.repo = repo;
    }

    public void SaveNewUser(string username) {
        var allAccounts = repo.GetAll();
        var found = allAccounts.FirstOrDefault(x => x.Username == username);
        if (found != null) {
            // errore
        } else {
            repo.Save(new UserAccount { Username = username, UserId = "aaa" });
        }
    }
}
