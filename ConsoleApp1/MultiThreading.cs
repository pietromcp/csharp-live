namespace ConsoleApp1; 

public class MultiThreading {
    private static Random Rnd = new Random();
    public static void Run() {
        Console.WriteLine("Thread Principale - start");
        const int threadCount = 100;
        Thread lastCreated = null;
        for (var i = 0; i < threadCount; i++) {
            Thread t = new Thread(() => {
                Thread.Sleep(Rnd.Next(100));
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} - start");
                Thread.Sleep(Rnd.Next(50));
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} - end");
            });
            if (i == threadCount - 1) {
                lastCreated = t;
            }

            t.Start();
        }

        lastCreated.Join();
        Console.WriteLine("Thread Principale - end " + Thread.CurrentThread.ManagedThreadId);

        // Thread t0 = new Thread(() => {
        //     Console.WriteLine("Thread 0 - start");
        //     // Thread.Sleep(5000);
        //     Console.WriteLine("Thread 0 - end");
        // });
        // Thread t1 = new Thread(() => {
        //     // Thread.Sleep(1000);
        //     Console.WriteLine("Thread 1 - start");
        //     // Thread.Sleep(2000);
        //     Console.WriteLine("Thread 1 - end");
        // });
        // t0.Start();
        // t1.Start();
    }
}

class Counter {
    private int value = 0;
    private object syncRoot = new object();

    public void Increment() {
        lock (syncRoot) {
            value++;
        }
        // lock (this) {
        //     value++;
        // }
    }

    public int Value => value;
}

public class CounterRunner {
    public static void Run() {
        const int threadCount = 100;
        const int incrementPerThread = 1000;
        var counter = new Counter();

        var executor = new ConcurrentExecutor {
            ThreadCount = threadCount,
            RepetitionsCount = incrementPerThread,
            RandomDelayBetweenExecutions = true
        };
        executor.Execute(() => counter.Increment());
        
        Console.WriteLine($"Actual counter value: {counter.Value}. Expected: {threadCount * incrementPerThread}");
    }
}

public class ConcurrentExecutor {
    public int ThreadCount { get; set; } = 50;
    public int RepetitionsCount { get; set; } = 200;
    public bool RandomDelayBetweenExecutions { get; set; } = true;

    private readonly Random random = new Random();

    public void Execute(Action action) {
        Barrier startBarrier = new Barrier(ThreadCount);
        Barrier endBarrier = new Barrier(ThreadCount + 1);
        for (int i = 0; i < ThreadCount; i++) {
            new Thread(() => {
                startBarrier.SignalAndWait();
                for (int j = 0; j < RepetitionsCount; j++) {
                    if (RandomDelayBetweenExecutions) {
                        Thread.Sleep(random.Next(10));
                    }
                    action();
                }
                endBarrier.SignalAndWait();
            }).Start();
        }
        endBarrier.SignalAndWait();
    }
}
