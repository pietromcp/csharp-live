namespace ConsoleApp1; 

public class TaskRunner {
    static Task CreateTask(string name) {
        return Task.Run(() => {
            Console.WriteLine($"Task {name} in thread {Thread.CurrentThread.ManagedThreadId}");
        }).ContinueWith(t => {
            Console.WriteLine($"Task {name} concluso in {Thread.CurrentThread.ManagedThreadId}");
        });
    }

    public static void Run() {
        var tasks = new List<Task>();
        for (var i = 0; i < 10; i++) {
            tasks.Add(CreateTask($"{i}"));
        }

        var whenAll = Task.WhenAll(tasks);
        whenAll.ContinueWith(t => {
            Console.WriteLine("Hanno finito tutti");
        });
        var whenAny = Task.WhenAny(tasks);
        whenAny.ContinueWith(t => {
            Console.WriteLine("Il primo ha finito");
        });
        Console.WriteLine("Metodo TaskRunner.Run concluso");
    }
}
