namespace ConsoleApp1; 

public class Parameters {
    public void Foo(int n) {
        Console.WriteLine($"n = {n}");
    }

    public int Sum(int x, int y) {
        return x + y;
    }
    
    public int Sum(int x, int y, int z) {
        return x + y + z;
    }

    public int Sum(int n0, params int[] ints) {
        int sum = n0;
        foreach (var i in ints) {
            sum += i;
        }
        return sum;
    }

    public void Foo(int n, string text, DateTimeOffset when, params string[] strings) {
    }

    public int Bar(int x, int y = 10) {
    // public int Bar(int x, int y) {
        return x * y;
    }
    
    // public int Bar(int x) {
    //     return Bar(x, 10);
    // }

    public void Aaa(int n, DateTimeOffset when) {
    }
    
    public void Aaa(int n) {
        Aaa(n, DateTimeOffset.Now);
    }

    public void DoPayment(string creditCardNumber, string confirmationEmail, string userId) {
    }
}
