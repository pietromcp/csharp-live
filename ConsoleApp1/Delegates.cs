namespace ConsoleApp1;

delegate int IntegerOperation(int x, int y);

public interface IntegerTransformationContainer {
    int Transform(int x);
}

public class MultiplyBy2 : IntegerTransformationContainer {
    public int Transform(int x) => x * 2;
}

public delegate int IntegerTransformation(int x);

public class Delegates {
    public static void Run() {
        var list = new List<int> { 1, 2, 3, 4, 5, 6 };
        var doubles = ApplyTransformation(list, x => x * 2);
        Print(doubles);
        var triples = ApplyTransformation(list, x => x * 3);
        Print(triples);
        var remaindersBy3 = ApplyTransformation(list, delegate(int i) { return i % 3; });
        Print(remaindersBy3);
        var result = ApplyTransformation(list, MyTransform);
        var divider2 = new Divider(2);
        var divider3 = new Divider(3);
        var remainders2 = ApplyTransformation(list, divider2.Apply);
        var remainders3 = ApplyTransformation(list, divider3.Apply);

        var resultUsingLinq = list.Select(x => x * 2);
        Print(resultUsingLinq);

        var listOfStrings = new[] { "pietrom", "pietro", "martinellip" };
        var lengths = listOfStrings.Select(x => x.Length);
        Print(lengths);
        var longStrings = listOfStrings.Where(x => x.Length > 6);
        Print(longStrings);

        var mappedInts = list
            .Select(x => x * 2) // map
            .Where(x => x > 7) // filter
            .Select(x => x * 3)
            // .First(x => x % 6 == 0)
            ;
        Print(mappedInts);

        var sumOfInts = list.Aggregate(0, (acc, curr) => acc + curr);
        var productOfInts = list.Aggregate(1, (acc, curr) => acc * curr);
        
        var enumerator = mappedInts.GetEnumerator();
        while (enumerator.MoveNext()) {
            Console.WriteLine("--> " + enumerator.Current);
        }

        foreach (var mappedInt in mappedInts) {
            Console.WriteLine("--> " + mappedInt);
        }
    }

    static void Print(IEnumerable<int> ints) {
        Console.WriteLine(string.Join(",", ints));
    }
    
    static void Print(IEnumerable<string> strings) {
        Console.WriteLine(string.Join(",", strings));
    }

    static int MyTransform(int a) {
        return a * a * a;
    }

    public static IList<int> ApplyTransformation(IList<int> ints, IntegerTransformation f) {
        var result = new List<int>();
        foreach (var i in ints) {
            result.Add(f(i));
        }
        return result;
    }
    
    public static IList<int> ApplyTransformation(IList<int> ints, IntegerTransformationContainer container) {
        var result = new List<int>();
        foreach (var i in ints) {
            result.Add(container.Transform(i));
        }
        return result;
    }
}

public class Divider {
    private int byN;

    public Divider(int byN) {
        this.byN = byN;
    }

    public int Apply(int n) {
        return n % byN;
    }
}
