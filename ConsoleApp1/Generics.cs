using System.Collections;

namespace ConsoleApp1;

public class Generics {
    public static void Run() {
        var strList = new List<string>();
        var intList = new List<int>();
        var dtoList = new List<DateTimeOffset>();
        var complexList = new List<Complex>();

        complexList.Add(new Complex(11, 17));
        intList.Add(19);
        strList.Add("pietrom");
        // strList.Add(19);

        IDictionary<int, string> httpStatusCodes = new Dictionary<int, string>();
        httpStatusCodes.Add(200, "Ok");
        httpStatusCodes.Add(404, "Not Found");
        httpStatusCodes.Add(401, "Unauthorized");
        httpStatusCodes.Add(418, "I'm a Teapot");
        httpStatusCodes.Add(500, "Internal Server Error");
        var iAmATeapot = httpStatusCodes[418];
        Console.WriteLine(iAmATeapot);
        Console.WriteLine(httpStatusCodes.ContainsKey(402));

        var complexToDateTimeOffset = new Dictionary<Complex, DateTimeOffset>();
        var peopleToInt = new Dictionary<Person, int>();
        var pietrom = new Person { FiscalCode = "MRTPTR" };
        var martinellip = new Person { FiscalCode = "MRTPTR" };
        Console.WriteLine(pietrom == martinellip);
        Console.WriteLine(pietrom == pietrom);
        peopleToInt.Add(pietrom, 19);
        peopleToInt.Add(martinellip, 22);
        Console.WriteLine(peopleToInt[pietrom]);
        Console.WriteLine(peopleToInt[martinellip]);

        complexToDateTimeOffset[new Complex(11, 19)] = DateTimeOffset.Now;
        complexToDateTimeOffset[new Complex(11, 19)] = DateTimeOffset.Now.AddDays(10);
        Console.WriteLine(complexToDateTimeOffset[new Complex(11, 19)]);

        var myList = new MyList<string>();
        var mappedList0 = myList.Map(x => x.Length);
        var mappedList1 = myList.Map(delegate(string x) { return x.Length; });
        var mappedList2 = myList.Map(x => x.ToUpper());

        var mappedList3 = myList.Map(new MyStringLengthMapper());
    }

    class MyStringLengthMapper : MyMapper<string, int> {
        public int Map(string input) {
            return input.Length;
        }
    }

    public class MyStrangeInterface<T1, T2, T3>
        where T1 : class, IComparable
        where T2 : IComparable<T3>
        where T3 : T2, new() {
    }

    public class Person {
        public string FiscalCode { get; set; }
    }

    public interface MyDictionary<TKey, TValue> {
        void Add(TKey key, TValue value);

        bool ContainsKey(TKey key);

        TValue this[TKey key] { get; }
    }

    public class MyBiDirectionalDictionary<T0, T1> {
        private IDictionary<T0, T1> t0ToT1 = new Dictionary<T0, T1>();
        private IDictionary<T1, T0> t1ToT0 = new Dictionary<T1, T0>();

        public void Add(T0 item0, T1 item1) {
            // t0ToT1.Add(item0, item1);
            t0ToT1[item0] = item1;
            // t1ToT0.Add(item1, item0);
            t1ToT0[item1] = item0;
        }

        public T1 this[T0 key] {
            get { return t0ToT1[key]; }
        }

        public T0 this[T1 key] {
            get { return t1ToT0[key]; }
        }
    }

    public interface MyCollectionInterface<T> {
        void Add(T item);

        bool Contains(T item);
    }

    public interface MyListInterface<T> : MyCollectionInterface<T> {
        T this[int index] { get; }
    }

    public class MyList<TItem> : IEnumerable<TItem> {
        public void Add(TItem item) {
        }

        public TItem this[int index] {
            get { return default(TItem); }
        }

        public MyList<TResult> Map<TResult>(MyMapperFunction<TItem, TResult> mapper) {
            var result = new MyList<TResult>();
            foreach (var item in this) {
                result.Add(mapper(item));
            }

            return result;
        }

        public MyList<TResult> Map<TResult>(MyMapper<TItem, TResult> mapper) {
            var result = new MyList<TResult>();
            foreach (var item in this) {
                result.Add(mapper.Map(item));
            }

            return result;
        }


        public IEnumerator<TItem> GetEnumerator() {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }

    public delegate TOut MyMapperFunction<TIn, TOut>(TIn input);

    public interface MyMapper<TIn, TOut> {
        TOut Map(TIn input);
    }

    public delegate int MyStringToIntMapperFunction(string input);

    public class MyStringList {
        public void Add(string item) {
        }
    }

    public class MyComplexList {
        public void Add(Complex item) {
        }
    }

    public class MyIntList {
        public void Add(int item) {
        }
    }

    public class ParentClass0 {
    }

    public class ParentClass1 {
    }

    public interface Interface0 {
    }

    public interface Interface1 {
    }

    public interface Interface2 {
    }

    public interface Interface3 {
    }

    public class ChildClass : ParentClass0, Interface0, Interface1, Interface2 {
    }

    public interface Interface4 : Interface0, Interface1, Interface2 {
    }
}
