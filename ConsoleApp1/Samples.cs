namespace ConsoleApp1; 

public class AClass : object, AnInterface {
    public void Foo() {
        Console.WriteLine("AClass");
    }
}

public class AStruct : object, AnInterface {
    public void Foo() {
        Console.WriteLine("AStruct");
    }
}

public record ARecord (string Text) : AnInterface {
    public void Foo() {
        Console.WriteLine("ARecord");
    }
}

public interface AnInterface {
    void Foo();
}