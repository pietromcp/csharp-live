namespace ConsoleApp1; 

public class MyHttpExample {
    public static void Run() {
        UseStatusCode(MyHttpStatusCode.NotFound);
        UseStatusCode(MyHttpStatusCode.IAmATeapot);
        UseStatusCode(MyHttpStatusCode.InternalServerError);
        // UseStatusCode(new MyHttpStatusCode(1000));
        // UseStatusCode((MyHttpStatusCode)1000);
    }

    private static void UseStatusCode(MyHttpStatusCode statusCode) {
        Console.WriteLine($"UseStatusCode {statusCode.Value}");
    }
}

internal class MyHttpStatusCode {
    private MyHttpStatusCode(int value) {
        Value = value;
    }

    public static MyHttpStatusCode IAmATeapot { get; } = new MyHttpStatusCode(418);
    public static MyHttpStatusCode NotFound { get; } = new MyHttpStatusCode(404);
    public static MyHttpStatusCode InternalServerError { get; } = new MyHttpStatusCode(500);
    public int Value { get; }
}


