namespace ConsoleApp1; 

public record Complex(decimal Re, decimal Im) {
    public static implicit operator Complex(decimal real) => new Complex(real, 0);
    
    public static Complex operator +(Complex left, Complex right) {
        return new Complex(left.Re + right.Re, left.Im + right.Im);
    }
    
    public static Complex operator *(Complex left, Complex right) {
        return new Complex(left.Re * right.Re - left.Im * right.Im, left.Re * right.Im + left.Im * right.Re);
    }
}
