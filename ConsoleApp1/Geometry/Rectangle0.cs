namespace ConsoleApp1.Geometry; 

public class Rectangle0 {
    private int width, height;

    public int Width {
        get {
            return width;
        }

        set {
            width = value;
        }
    }

    public int Witdh2 { get; }

    public Rectangle0(int width, int height) {
        this.width = width;
        Witdh2 = width;
        this.height = height;
    }

    //public int Area => width * height;
    public int Area {
        get {
            return width * height;
        }
    }

    // public int GetArea() => width * height;
    public int GetArea() {
        return width * height;
    }

    public int Perimeter => 2 * (width + height);
    
    public int GetPerimeter() => 2 * (width + height);

    public double GetVeryComplicatedValue(int n, double d, string s, DateTime when) {
        if (when.Millisecond == 0) {
            return 19;
        }
        return (n / 1.2) + Math.Cos(d) + Math.Sqrt(s.Length) / when.Millisecond;
    }
}
