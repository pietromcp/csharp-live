namespace ConsoleApp1.Geometry; 

public interface Shape {
    double Area { get; }
    double Perimeter { get; }
}

public class Rectangle : Shape {
    public Rectangle(double width, double height) {
        Width = width;
        Height = height;
    }

    public double Width { get; }
    public double Height { get; }
    public double Area => Width * Height;
    public double Perimeter => 2 * (Width + Height);
}

public class Circle : Shape {
    public Circle(double radius) {
        Radius = radius;
    }

    public double Radius { get; }
    public double Area => Math.PI * Radius * Radius;
    public double Perimeter => 2 * Math.PI * Radius;
}