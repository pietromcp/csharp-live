namespace ConsoleApp1.Zoo;

public interface EssereVivente {
    void Ping();
    
    void Pong();
}

public abstract class Animale : EssereVivente {
    public void Ping() {
        Console.WriteLine("EssereVivente.Ping");
    }

    public abstract void Pong();
}

public abstract class Mammifero : Animale {
    protected Mammifero(DateOnly birthDay) {
        Console.WriteLine("new Mammifero");
    }

    public abstract void Foo();

    public virtual void Bar() {
        Console.WriteLine("Mammifero.Bar");
    }

    public void Abc() {
        Console.WriteLine("Mammifero.Abc");
    }

    public override void Pong() {
        Console.WriteLine("Mammifero.Pong");
    }
}

public class Gatto : Mammifero {
    public Gatto(DateOnly birthDay) : base(birthDay) {
        Console.WriteLine("new Gatto");
    }

    public override void Foo() {
        Console.WriteLine("Gatto.Foo");
    }

    public override void Bar() {
        Console.WriteLine("Gatto.Bar");
    }

    public override void Pong() {
        Console.WriteLine("Gatto.Pong");
    }
}

public class GattoEuropeo : Gatto {
    public GattoEuropeo(DateOnly birthDay) : base(birthDay) {
    }

    public override void Bar() {
        Console.WriteLine("GattoEuropeo.Bar");
    }
}

public class Leone : Mammifero {
    public Leone(DateOnly birthDay) : base(birthDay) {
        Console.WriteLine("new Leone");
    }

    public override void Foo() {
        Console.WriteLine("Leone.Foo");
    }
    
    public override void Bar() {
        Console.WriteLine("Leone.Bar");
    }
    
    public new void Abc() {
        Console.WriteLine("Leone.Abc");
    }
}
