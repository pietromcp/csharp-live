﻿// See https://aka.ms/new-console-template for more information

using ConsoleApp1;
using ConsoleApp1.Geometry;
using ConsoleApp1.Zoo;

// Console.WriteLine("Hello, World!");
//
// var c0 = new Complex(11, 19);
// var c1 = new Complex(19, 11);
// Complex c2 = 17.22M;
//
// Console.WriteLine(c0);
// Console.WriteLine(c1);
// Console.WriteLine(c2);
// Console.WriteLine(c0 + c1 + c2);
// Console.WriteLine(1.1M * c0);
//
// var p = new Piece0();
// p.Foo();
// p.Bar();
//
// var list = new AnInterface[] { new ARecord("pietrom"), new AClass(), new AStruct() };
// foreach (var anInterface in list) {
//     anInterface.Foo();
// }
//
// var pars = new Parameters();
// pars.Foo(19);
// var sum = pars.Sum(11, 19);
// var sum3 = pars.Sum(11, 19, 17);
// var sumArray = pars.Sum(0, new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
// var sum6 = pars.Sum(1, 2, 3, 4, 5, 6);
// var sum4 = pars.Sum(1, 2, 3, 4);
// var sum8 = pars.Sum(1, 2, 3, 4, 5, 6, 7, 8);
// Console.WriteLine($"sum = {sum}");
// Console.WriteLine($"sum3 = {sum3}");
// Console.WriteLine($"sumArray = {sumArray}");
//
// Console.WriteLine($"sum6 = {sum6}");
// Console.WriteLine($"sum4 = {sum4}");
// Console.WriteLine($"sum8 = {sum8}");
// Console.WriteLine($"sum0 = {pars.Sum(0)}");
//
// pars.Foo(11, "pietromm", DateTimeOffset.Now);
// pars.Foo(11, "pietromm", DateTimeOffset.Now, new string[] { "a", "b" });
// pars.Foo(11, "pietromm", DateTimeOffset.Now, "a", "b");
//
// pars.Bar(11, 19);
// pars.Bar(11);
// pars.Bar(11, 10);
//
// pars.Aaa(when: DateTimeOffset.MaxValue, n: 11);
//
// pars.DoPayment(
//     userId: "7683247683254876",
//     creditCardNumber: "98734589734589",
//     confirmationEmail: "pietro@martinelli.com"
// );
//
// var r0 = new Rectangle0(11, 19);
// Console.WriteLine($"AREA = {r0.Area}");
// Console.WriteLine($"PERIMETER = {r0.GetPerimeter()}");
//
// var l0 = new BritishLength(1, 1, 1);
// var l1 = new BritishLength(11, 2, 11);
// var lSum = l0.Add(l1);
// Console.WriteLine($"({lSum.Yards}, {lSum.Feet}, {lSum.Inches})");
// var lSub = l1.Sub(l0);
// Console.WriteLine($"({lSub.Yards}, {lSub.Feet}, {lSub.Inches})");
//
// var a0 = new UserAccount("pietrom");
// var a1 = new UserAccount("pietro");
// var a2 = new UserAccount("martinellip");
// var a3 = new UserAccount("martinelli");
// var a4 = new UserAccount("pietromartinelli");
//
// Console.WriteLine(a0);
// Console.WriteLine(a1);
// Console.WriteLine(a2);
// Console.WriteLine(a3);
// Console.WriteLine(a4);
//
// new Constructors(11);
// new Constructors();
// new Constructors();
// new Constructors();
// new Constructors(17);
// new Constructors();
// new Constructors(22);
// new Constructors();
// new Constructors(6);
// new Constructors();
// new Constructors(21);
//
// MyHttpExample.Run();
//
// string text = "pietro";
// Console.WriteLine(text.Times(5));
// Console.WriteLine(StringExtension.Times(text, 5));
// List<string> strList = null;
// Console.WriteLine(strList.OrEmpty()
//     .Count);
//
// var myClass = new MyClass();
// myClass["pietrom", 1] = 19;
// Console.WriteLine(myClass["pietrom", 1]);
// Console.WriteLine(myClass["pietrom", 2].HasValue);
// myClass["xx", 19] = null;
// myClass["pietrom", 1] = 22;
// Console.WriteLine(myClass["pietrom", 1]);
//
// Delegates.Run();
//
// Mammifero leone = new Leone(new DateOnly(1978, 3, 19));
// leone.Foo();
// Mammifero gatto = new Gatto(new DateOnly(1978, 3, 19));
// gatto.Foo();
// Console.WriteLine("---------------------");
// var mammiferi = new Mammifero[] { leone, gatto, new GattoEuropeo(new DateOnly(1978, 3, 19)) };
// foreach (var m in mammiferi) {
//     Console.WriteLine("*** " + (m is Gatto g));
//     m.Foo();
//     m.Bar();
//     m.Abc();
// }
// Console.WriteLine("---------------------");
// Leone leoneConsapevole = leone as Leone;
// leoneConsapevole.Abc();

// Generics.Run();
// Files.Run();
MultiThreading.Run();
// CounterRunner.Run();
// TaskRunner.Run();

static class EnumerableExtension {
    public static IList<string> OrEmpty(this IList<string> self) {
        return self != null ? self : new List<string>();
    }
}

static class StringExtension {
    public static string Times(this string txt, int n) {
        return Enumerable.Range(1, n)
            .Aggregate("", (acc, curr) => acc + txt);
    }
}

public class BritishLength {
    private int totalInches;

    public BritishLength(int y, int f, int i) : this(i + f * 12 + y * 3 * 12) {
    }

    private BritishLength(int totalInches) {
        this.totalInches = totalInches;
    }

    public int Yards => totalInches / (12 * 3);
    public int Feet => (totalInches % (12 * 3)) / 12;
    public int Inches => totalInches % 12;

    public BritishLength Add(BritishLength that) {
        return new BritishLength(this.totalInches + that.totalInches);
    }

    public BritishLength Sub(BritishLength that) {
        return new BritishLength(this.totalInches - that.totalInches);
    }

    public BritishLength Mul(int n) {
        return new BritishLength(this.totalInches * n);
    }

    public (BritishLength, BritishLength) Div(int n) {
        return (
            new BritishLength(this.totalInches / n),
            new BritishLength(this.totalInches % n)
        );
    }
}

public class UserAccount {
    private static int NextId = 0;
    private int Width = 0;

    public UserAccount(string username) {
        Username = username;
        Id = NextId++;
    }

    public int Id { get; }
    public string Username { get; }

    public override string ToString() {
        return $"{Id} : {Username} (NextId: {NextId})";
    }
}

class Constructors {
    private int value;

    public Constructors(int value) {
        Console.WriteLine("Constructors(int) " + value);
        this.value = value;
    }

    public Constructors() : this(19) {
        Console.WriteLine("Constructors()");
    }

    static Constructors() {
        Console.WriteLine("static Constructors()");
    }
}

class Constructors2 {
    private int value;

    public Constructors2(string text, int value) {
        this.value = value;
    }

    public Constructors2(string text) : this(text, text.Length * 3) {
        Console.WriteLine("Constructors()");
    }
}

class MyClass {
    private IList<(string, int, int)> values = new List<(string, int, int)>();
    
    public int? this[string key, int n] {
        get {
            Console.WriteLine($"GET key = {key} n = {n}");
            foreach (var v in values) {
                if (v.Item1 == key && v.Item2 == n) return v.Item3;
            }
            return null;
        }
        set {
            Console.WriteLine($"SET key = {key} n = {n} value = {value}");
            if (value.HasValue) {
                values.Add((key, n, value.Value));
            }
        }
    }
}
