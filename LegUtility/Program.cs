﻿using System.Text.RegularExpressions;

class Program {
    
    static async Task Main() {
        var lines = await File.ReadAllLinesAsync("example-0.leg");
        short numOfLines = short.Parse(lines[1]
            .TrimStart()
            .Split(' ')[0]);
        Console.WriteLine($"Numero di linee: {numOfLines}");

        var positions = lines
            .Skip(2)
            .Take(numOfLines)
            .Select(x => x.TrimStart())
            .Select(x => new Regex("  +").Replace(x, " "))
            .Select(x => x.Split(' ').Take(2).ToArray())
            .Select(x => new Position(short.Parse(x[0].Substring(0, x[0].IndexOf('.'))), float.Parse(x[1])));
        
        Console.WriteLine(String.Join("\n", positions));


        short masterOffset = 85;

        var newPositions = positions.Skip(masterOffset)
            .Concat(positions.Take(masterOffset))
            .Select(x => new Position(translateMasterPositions(masterOffset, x.Master, numOfLines), x.Slave));
        
        Console.WriteLine("Stampa newPositions:");
        Console.WriteLine(String.Join("\n", newPositions));
        
        
        
    }

    static private short translateMasterPositions(short masterOffset, short masterPos, short numOfLines) {
        return (short)((masterPos - masterOffset + numOfLines) % numOfLines);
    }
}

public record Position(short Master, float Slave);

